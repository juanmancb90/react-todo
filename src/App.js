import React, { Component } from 'react';
import TodoApp from './components/TodoApp';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>TodoList with react</h2>
        </div>
        <div className="App-intro">
          <TodoApp />
        </div>
      </div>
    );
  }
}

export default App;
